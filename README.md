# CPEG222 Pong Project
------
This project uses MPLAB X IDE v5.40.

It has been written for the PIC32MX370F512L chip (Basys MX3 board).

Requires the following Basys MX3 library sources:

 - config.h
 - utils.h/utils.c
 - lcd.h/lcd.c
