/*===================================CPEG222====================================
 * Program:		CPEG222_demo_sample.c
 * Authors: 	Chengmo Yang
 * Date: 		8/30/2020
 * Description: demo_sample uses on-board BTNs to control the lighting sequence of
 * the on-board LEDs 0 through 7.  The initial state is LD0 ON.
 * Input: Button press
 * Output: LED is turned on and off one by one.
==============================================================================*/
/*------------------ Board system settings. PLEASE DO NOT MODIFY THIS PART ----------*/
#ifndef _SUPPRESS_PLIB_WARNING          //suppress the plib warning during compiling
    #define _SUPPRESS_PLIB_WARNING      
#endif
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
#pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
#pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)
#pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config POSCMOD = XT             // Primary Oscillator Configuration (XT osc mode)
#pragma config FPBDIV = DIV_8           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/8)
/*----------------------------------------------------------------------------*/
     
#include <xc.h>   //Microchip XC processor header which links to the PIC32MX370512L header
#include "config.h" // Basys MX3 configuration header
#include "lcd.h"

/* --------------------------- Forward Declarations-------------------------- */
void delay_ms(int ms);

/* -------------------------- Definitions------------------------------------ */
#define SYS_FREQ    (80000000L) // 80MHz system clock
int LD_position = 0x01;         // Initially turn on LD0, you can also do LD_position = 1;
int buttonLock = 0;             // Variable to "lock" the button
int delay = 20;                // A delay variable of 20 ms
/* ----------------------------- Main --------------------------------------- */

int main(void) 
{
 /*-------------------- Port and State Initialization -------------------------*/
   DDPCONbits.JTAGEN = 0;      // Statement is required to use Pin RA0 as IO
    TRISA &= 0xFF00;			// Set Port A bits 0~7 to 0, i.e., LD 0~7 are configured as digital output pins
    TRISFbits.TRISF0 = 1;       // Set RF0 to 1, i.e., BTNC is configured as input  
    LATA = LD_position;        // write the hex value of LD_position to the 8 LEDs
    LCD_Init();                 // Initialize the LCD

    int btnC_val;               // Variable to read button C
    LCD_WriteStringAtPos("Welcome", 0, 4);            //Display "Welcome" at line 0, position 4
    LCD_WriteStringAtPos("Press BTNC", 1, 0);       //Display "Press BTNC" at line 1, position 0

    while (1) 
    {
/*-------------------- Main logic and actions start --------------------------*/
        btnC_val = PORTFbits.RF0;   // read BTNC status
        
        if(btnC_val && !buttonLock)    // Actions when central button is pressed
        {
            delay_ms(delay);        // wait 20ms, or you can change the value of delay
                                    // to make the button more/less sensitive
            buttonLock = 1;
            
            if (LD_position != 0x80)  // if LD7 is not on
                LD_position = LD_position << 1; // Turn on left LD and turn off current one
            else                      // LD7 is on, start over
                LD_position = 0x01;
 
            LATA = LD_position; //sets the value for all 8 LEDs
        }
        else if(buttonLock && !btnC_val) // Actions when the central button is released
                                             
        {
            delay_ms(delay);        // wait 20ms, or you can change the value of delay
                                    // to make the button more/less sensitive
            buttonLock = 0;         //unlock buttons to allow further press being recognized
        }
    }
/*--------------------- Action and logic end ---------------------------------*/
}
/* ----------------------------------------------------------------------------- 
**	delay_ms
**	Parameters:
**		ms - amount of milliseconds to delay (based on 80 MHz SSCLK)
**	Return Value:
**		none
**	Description:
**		Create a delay by counting up to counter variable
** -------------------------------------------------------------------------- */
void delay_ms(int ms){
	int		i,counter;
	for (counter=0; counter<ms; counter++){
        for(i=0;i<1426;i++){}   //software delay 1 millisec
    }
}


